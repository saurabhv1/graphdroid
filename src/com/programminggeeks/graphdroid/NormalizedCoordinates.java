package com.programminggeeks.graphdroid;

public class NormalizedCoordinates {
	
	float x;
	
	float y;
	
	public NormalizedCoordinates(float x, float y) {
		if(x > 100) 
			x = x % 100;
		if(y > 100)
			y = y % 100;
		this.x = x;
		this.y = y;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

}
