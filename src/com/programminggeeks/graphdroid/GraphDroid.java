package com.programminggeeks.graphdroid;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;

import com.example.myandroidgraph.R;
import com.programminggeeks.bellmanford.BellmanFord;
import com.programminggeeks.dijkstra.Dijkstra;
import com.programminggeeks.dijkstra.DijkstraNode;
import com.programminggeeks.floydwarshall.FloydWarshall;
import com.programminggeeks.graph.Edge;
import com.programminggeeks.graph.Graph;
import com.programminggeeks.graph.Node;
import com.programminggeeks.graph.WeightedDirectionalEdge;
import com.programminggeeks.util.GraphUtils;

public class GraphDroid extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		GraphUtils.init(this);
		
		//Graph graph = createDijkstraGraph();//createGraph();
		/*Dijkstra dj = new Dijkstra();
		dj.runDijkstra();*/
		
		/*BellmanFord bf = new BellmanFord();
		bf.runBellmanFord();*/
		
		FloydWarshall fw = new FloydWarshall();
		fw.runFloydMarshall();
		
		//setContentView(new GraphView(this,graph));
		setContentView(R.layout.activity_my_android_graph);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.my_android_graph, menu);
		return true;
	}
	
	private Graph createDijkstraGraph() {
		Graph graph = new Graph();
		
		List<DijkstraNode> nodes = new ArrayList<DijkstraNode>();
		
		DijkstraNode n1 = new DijkstraNode(new NormalizedCoordinates(30,40));
		n1.setNodeLabel("1"); nodes.add(n1);
		DijkstraNode n2 = new DijkstraNode(new NormalizedCoordinates(10,10));
		n2.setNodeLabel("2"); nodes.add(n2);
		DijkstraNode n3 = new DijkstraNode(new NormalizedCoordinates(90,80));
		n3.setNodeLabel("3"); nodes.add(n3);
		DijkstraNode n4 = new DijkstraNode(new NormalizedCoordinates(50,50));
		n4.setNodeLabel("4"); nodes.add(n4);
		
		DijkstraNode n5 = new DijkstraNode(new NormalizedCoordinates(70,10));
		n5.setNodeLabel("5"); nodes.add(n5);
		DijkstraNode n6 = new DijkstraNode(new NormalizedCoordinates(40,80));
		n6.setNodeLabel("6"); nodes.add(n6);
		DijkstraNode n7 = new DijkstraNode(new NormalizedCoordinates(10,35));
		n7.setNodeLabel("7"); nodes.add(n7);
		DijkstraNode n8 = new DijkstraNode(new NormalizedCoordinates(25,75));
		n8.setNodeLabel("8"); nodes.add(n8);
		DijkstraNode n9 = new DijkstraNode(new NormalizedCoordinates(45,80));
		n9.setNodeLabel("9"); nodes.add(n9);
		
		graph.addNodes(nodes);
		
		List<WeightedDirectionalEdge> edges = new ArrayList<WeightedDirectionalEdge>();
		
		edges.add(new WeightedDirectionalEdge(n1,n2,10,1));
		edges.add(new WeightedDirectionalEdge(n4, n3,5,1));
		edges.add(new WeightedDirectionalEdge(n5, n6,8,1));
		edges.add(new WeightedDirectionalEdge(n8, n6,8,1));
		edges.add(new WeightedDirectionalEdge(n4, n2,8,1));
		edges.add(new WeightedDirectionalEdge(n6, n4,8,1));
		edges.add(new WeightedDirectionalEdge(n9, n5,8,1));
		edges.add(new WeightedDirectionalEdge(n8, n7,8,1));
		edges.add(new WeightedDirectionalEdge(n1, n9,8,1));
		
		edges.add(new WeightedDirectionalEdge(n2, n5,8,1));
		edges.add(new WeightedDirectionalEdge(n5, n3,8,1));
		
		graph.addEdges(edges);
		graph.createAdjacencyList();
		
		for(Node node : nodes)
			Log.i("K","Node: "+node.getNodeLabel()+" Degree: "+node.getDegree());
		
		return graph;
	}
	
	private Graph createGraph() {
		Graph graph = new Graph();
		
		List<Node> nodes = new ArrayList<Node>();
		
		Node n1 = new Node(new NormalizedCoordinates(30,40));
		n1.setNodeLabel("1"); nodes.add(n1);
		Node n2 = new Node(new NormalizedCoordinates(10,10));
		n2.setNodeLabel("2"); nodes.add(n2);
		Node n3 = new Node(new NormalizedCoordinates(90,80));
		n3.setNodeLabel("3"); nodes.add(n3);
		Node n4 = new Node(new NormalizedCoordinates(50,50));
		n4.setNodeLabel("4"); nodes.add(n4);
		
		Node n5 = new Node(new NormalizedCoordinates(70,10));
		n5.setNodeLabel("5"); nodes.add(n5);
		Node n6 = new Node(new NormalizedCoordinates(40,80));
		n6.setNodeLabel("6"); nodes.add(n6);
		Node n7 = new Node(new NormalizedCoordinates(10,35));
		n7.setNodeLabel("7"); nodes.add(n7);
		Node n8 = new Node(new NormalizedCoordinates(25,75));
		n8.setNodeLabel("8"); nodes.add(n8);
		Node n9 = new Node(new NormalizedCoordinates(45,80));
		n9.setNodeLabel("9"); nodes.add(n9);
		
		graph.addNodes(nodes);
		
		List<Edge> edges = new ArrayList<Edge>();
		
		edges.add(new Edge(n1,n2));
		edges.add(new Edge(n4, n3));
		edges.add(new Edge(n5, n6));
		edges.add(new Edge(n8, n6));
		edges.add(new Edge(n4, n2));
		edges.add(new Edge(n6, n4));
		edges.add(new Edge(n9, n5));
		edges.add(new Edge(n8, n7));
		edges.add(new Edge(n1, n9));
		
		edges.add(new Edge(n2, n5));
		edges.add(new Edge(n5, n3));
		
		graph.addEdges(edges);
		
		graph.createAdjacencyList();
		
		for(Node node : nodes)
			Log.i("K","Node: "+node.getNodeLabel()+" Degree: "+node.getDegree());
		
		return graph;
	}

}
