package com.programminggeeks.bellmanford;

import com.programminggeeks.dijkstra.DijkstraNode;
import com.programminggeeks.graph.Node;
import com.programminggeeks.graphdroid.NormalizedCoordinates;

public class BellmanFordNode extends Node {

	/*
	 * It will have the shortest distance value
	 * from the Source Node to this Node
	 */
	public double d;
	
	/*
	 * The node which is just before the current node
	 * in the shortest path
	 */
	public BellmanFordNode predecessor;
	
	
	public BellmanFordNode(NormalizedCoordinates coordinates) {
		super(coordinates);
		// TODO Auto-generated constructor stub
	}

}
