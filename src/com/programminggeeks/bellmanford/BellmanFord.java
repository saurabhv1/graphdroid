package com.programminggeeks.bellmanford;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.programminggeeks.graph.Edge;
import com.programminggeeks.graph.Graph;
import com.programminggeeks.graph.Node;
import com.programminggeeks.graph.WeightedDirectionalEdge;
import com.programminggeeks.graphdroid.NormalizedCoordinates;
import com.programminggeeks.util.Log;


/**
 * This algorithm solves the 'Single Source shortest path' problem
 * in the general case in which edge weights may be negative.
 * However, there should not be a negative weight cycle that is 
 * reachable from the source.
 * @author saurabh
 *
 */
public class BellmanFord {
	
	private final float INFINITY = Float.POSITIVE_INFINITY;
	
	private static final String TAG = BellmanFord.class.getSimpleName();
	
	private Graph graph;
	
	private BellmanFordNode sourceNode;
	
	/*
	 * The list of nodes for which we have determined
	 * the shortest path from the sourceNode 
	 */
	//private List<BellmanFordNode> bellmenFordNodes = new ArrayList<BellmanFordNode>();
	
	public BellmanFord() {
		this.graph = buildSampleGraphForBellmanFord();
	}
	
	
	public void runBellmanFord() {
		init();
		boolean pathBuild = buildShortestPath();
		Log.i(TAG,"Path build is: "+pathBuild);
		if(pathBuild)
			printShortestPath();
	}
	
	private void init() {
		// set the 'd' parameter for all the vertices (nodes) as INFINITY and 
		// that of source as 0
		List<Node> nodes = graph.getNodes();
		for(Node node : nodes) {
			if(!node.equals(sourceNode))
				((BellmanFordNode)node).d = INFINITY;
			else
				((BellmanFordNode)node).d = 0;
		}
	}
	
	private boolean buildShortestPath() {
		for(Node node : graph.getNodes()) {
			for(Edge edge : graph.getEdges()) {
				Node[] bfNode = edge.getNodes();
				relax((BellmanFordNode)bfNode[0], (BellmanFordNode)bfNode[1], ((WeightedDirectionalEdge)edge).getWeight());
			}
		}
		
		// Check for negative cycle
		for(Edge edge : graph.getEdges()) {
			Node[] bfNode = edge.getNodes();
			if(((BellmanFordNode)bfNode[1]).d > ((BellmanFordNode)bfNode[0]).d + ((WeightedDirectionalEdge)edge).getWeight())
				return false;
		}
		return true;
	}
	
	private void printShortestPath() {
		for(Node node : graph.getNodes()) {
			Log.i(TAG,"Shortest distance for "+node.getNodeLabel()+":"+((BellmanFordNode)node).d);
			BellmanFordNode predecessorNode = (BellmanFordNode)node;
			Log.i(TAG,"Path for "+node.getNodeLabel());
			List<BellmanFordNode> nodesInShortestPath = new ArrayList<BellmanFordNode>();
			while(predecessorNode != null) {
				//Log.i(TAG,predecessorNode.getNodeLabel());
				nodesInShortestPath.add(predecessorNode);
				predecessorNode = predecessorNode.predecessor;
			}
			Collections.reverse(nodesInShortestPath);
			for(BellmanFordNode preNode : nodesInShortestPath)
				Log.i(TAG,preNode.getNodeLabel());
		}
	}
	
	/**
	 * Update the value of v.d if u.d+w(u,v) < v.d 
	 * @param u - the start node
	 * @param v - the end node
	 * @param w - the weight of the edge u->v
	 */
	private void relax(BellmanFordNode u, BellmanFordNode v, double w) {
		if(v.d > u.d+w) {
			v.d = u.d + w;
			v.predecessor = u;
		}
	}
	
	
	public Graph buildSampleGraphForBellmanFord() {
		
		Graph graph = new Graph();
		
		int numNodes = 5;
		List<BellmanFordNode> nodes = new ArrayList<BellmanFordNode>();
		List<WeightedDirectionalEdge> edges = new ArrayList<WeightedDirectionalEdge>();
		
		// create 5 nodes
		for(int x=0;x<numNodes;x++) {
			nodes.add(new BellmanFordNode(new NormalizedCoordinates(new Random().nextInt(90),new Random().nextInt(90))));
			nodes.get(x).setNodeLabel(x+"");
		}
		
		// create edges
		WeightedDirectionalEdge edge = new WeightedDirectionalEdge(nodes.get(0),nodes.get(1),6,1);
		edge.setLabel(0+""); edges.add(edge);
		edge = new WeightedDirectionalEdge(nodes.get(0),nodes.get(4),7,1);
		edge.setLabel(1+""); edges.add(edge);
		
		edge = new WeightedDirectionalEdge(nodes.get(1),nodes.get(2),5,1);
		edge.setLabel(2+""); edges.add(edge);
		edge = new WeightedDirectionalEdge(nodes.get(1),nodes.get(3),-4,1);
		edge.setLabel(3+""); edges.add(edge);
		edge = new WeightedDirectionalEdge(nodes.get(1),nodes.get(4),8,1);
		edge.setLabel(4+""); edges.add(edge);
		
		edge = new WeightedDirectionalEdge(nodes.get(2),nodes.get(1),-2,1);
		edge.setLabel(5+""); edges.add(edge);
		
		edge = new WeightedDirectionalEdge(nodes.get(3),nodes.get(2),7,1);
		edge.setLabel(6+""); edges.add(edge);
		edge = new WeightedDirectionalEdge(nodes.get(3),nodes.get(0),2,1);
		edge.setLabel(7+""); edges.add(edge);
		
		edge = new WeightedDirectionalEdge(nodes.get(4),nodes.get(3),9,1);
		edge.setLabel(8+""); edges.add(edge);
		edge = new WeightedDirectionalEdge(nodes.get(4),nodes.get(2),-3,1);
		edge.setLabel(8+""); edges.add(edge);
		
		graph.addNodes(nodes);
		graph.addEdges(edges);
		graph.createAdjacencyList();
		this.sourceNode = nodes.get(0);
		
		return graph;
	}

}
