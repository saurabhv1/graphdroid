package com.programminggeeks.util;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

public class GraphUtils {
	
	private static Context mContext;
	
	private static float screenHeight;
	
	private static float screenWidth;
	
	
	public static void init(Context context) {
		mContext = context;
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		
		screenHeight = display.getHeight();
		screenWidth = display.getWidth();
	}
	
	public static float convertToDp(float px) {
		Resources resources = mContext.getResources();
	    DisplayMetrics metrics = resources.getDisplayMetrics();
	    float dp = px / (metrics.densityDpi / 160f);
	    return dp;
	}
	
	public static float getScreenHeight() {
		return screenHeight;
	}
	
	public static float getScreenWidth() {
		return screenWidth;
	}
	
	
	public static float toPhoneXCoordinate(float x) {
		return (x/100 * GraphUtils.getScreenWidth());
	}
	
	public static float toPhoneYCoordinate(float y) {
		return (y/100 * GraphUtils.getScreenHeight());
	}

}
