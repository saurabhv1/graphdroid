package com.programminggeeks.util;

public class Log {
	
	private static final String PREFIX = "GraphDroid-";
	
	private static final boolean LOG_VERBOSE = false;
	
	private static final boolean LOG_INFO = true;
	
	private static final boolean LOG_DEBUG = true;
	
	private static final boolean LOG_ERROR = true;
	
	public static void v(String tag,String msg) {
		if(LOG_VERBOSE)
			Log.v(PREFIX+tag, msg);
	}
	
	public static void i(String tag,String msg) {
		if(LOG_INFO)
			android.util.Log.i(PREFIX+tag, msg);
	}
	
	public static void d(String tag,String msg) {
		if(LOG_DEBUG)
			android.util.Log.d(PREFIX+tag, msg);
	}
	
	public static void e(String tag,String msg) {
		if(LOG_ERROR)
			android.util.Log.e(PREFIX+tag, msg);
	}

}
