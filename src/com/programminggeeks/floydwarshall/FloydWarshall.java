package com.programminggeeks.floydwarshall;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.programminggeeks.bellmanford.BellmanFordNode;
import com.programminggeeks.graph.Edge;
import com.programminggeeks.graph.Graph;
import com.programminggeeks.graph.Node;
import com.programminggeeks.graph.WeightedDirectionalEdge;
import com.programminggeeks.graphdroid.NormalizedCoordinates;
import com.programminggeeks.util.Log;

/**
 * This algorithm computes All-Pair shortest path using Dynamic Programming. <br>
 * The recursive relation used is: <br>
 * d(i,j),k = w(i,j) if k = 0 <br>
 * d(i,j),k = min(d(i,j),(k-1) AND d(i,k),(k-1)+d(k,j),(k-1)) if k >= 1 <br>
 * Here, d(i,j),k is the weight of a shortest path from node i to node j
 * for which all the intermediate nodes are in the set {1,2,..k}. <br>
 * 
 * The algorithm runs in Theta(n^3) time
 * 
 * @author saurabh
 *
 */
public class FloydWarshall {

	private final float INFINITY = Float.POSITIVE_INFINITY;

	private static final String TAG = FloydWarshall.class.getName();
	
	private Graph graph;

	public FloydWarshall() {
		this.graph = buildSampleGraphForFlyodWarshall();
	}


	public void runFloydMarshall() {
		int n = graph.getNodes().size();
		
		double dist[][] = new double[n][n];	// This matrix will contain the shortest path weights
		for(int r=0; r<n;r++)
			for(int c=0; c<n;c++)
				dist[r][c] = INFINITY;
		
		String path[][] = new String [n][n]; // This matrix will contain the shortest path 
		
		for(int x=0;x<n;x++)
			dist[x][x]= 0;
		
		for(Edge edge : graph.getEdges()) {
			Node nodes[] = edge.getNodes();
			int r = Integer.parseInt(nodes[0].getNodeLabel());
			int c = Integer.parseInt(nodes[1].getNodeLabel());
			dist[r][c] = ((WeightedDirectionalEdge)edge).getWeight();
			if(r != c)
				path[r][c] = r+"";
		}
		
		for(int k=0; k<n; k++) {
			for(int i=0;i<n; i++) {
				for(int j=0; j<n; j++) {
					if (dist[i][j] > dist[i][k] + dist[k][j]) {
						dist[i][j] = dist[i][k] + dist[k][j];
						path[i][j] = path[k][j];
					}
				}
			}
		}

		for(int r=0; r<n ;r++) {
			for(int c=0; c<n; c++) {
				Log.i(TAG,dist[r][c]+" ");
			}
			Log.i(TAG,"\n");
		}
		
		for(int r=0; r<n ;r++) {
			for(int c=0; c<n; c++) {
				Log.i(TAG,path[r][c]+" ");
			}
			Log.i(TAG,"\n");
		}
	}

	private float min(float w1,float w2) {
		if(w1 > w2)
			return w1;
		return w2;
	}

	/**
	 * Exampe graph taken from Corman#FLoyd-Warshall Algorithm
	 * @return Weighted directed acyclic graph
	 */
	public Graph buildSampleGraphForFlyodWarshall() {

		Graph graph = new Graph();

		int numNodes = 5;
		List<Node> nodes = new ArrayList<Node>();
		List<WeightedDirectionalEdge> edges = new ArrayList<WeightedDirectionalEdge>();

		// create 5 nodes
		for(int x=0;x<numNodes;x++) {
			nodes.add(new Node(new NormalizedCoordinates(new Random().nextInt(90),new Random().nextInt(90))));
			nodes.get(x).setNodeLabel(x+"");
		}

		// create edges
		WeightedDirectionalEdge edge = new WeightedDirectionalEdge(nodes.get(0),nodes.get(1),3,1);
		edges.add(edge);
		edge = new WeightedDirectionalEdge(nodes.get(0),nodes.get(2),8,1);
		edges.add(edge);
		edge = new WeightedDirectionalEdge(nodes.get(0),nodes.get(4),-4,1);
		edges.add(edge);

		edge = new WeightedDirectionalEdge(nodes.get(1),nodes.get(3),1,1);
		edges.add(edge);
		edge = new WeightedDirectionalEdge(nodes.get(1),nodes.get(4),7,1);
		edges.add(edge);

		edge = new WeightedDirectionalEdge(nodes.get(2),nodes.get(1),4,1);
		edges.add(edge);

		edge = new WeightedDirectionalEdge(nodes.get(3),nodes.get(2),-5,1);
		edges.add(edge);
		edge = new WeightedDirectionalEdge(nodes.get(3),nodes.get(0),2,1);
		edges.add(edge);

		edge = new WeightedDirectionalEdge(nodes.get(4),nodes.get(3),6,1);
		edges.add(edge);

		graph.addNodes(nodes);
		graph.addEdges(edges);
		graph.createAdjacencyList();

		return graph;
	}

}
