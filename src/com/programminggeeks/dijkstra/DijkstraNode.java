package com.programminggeeks.dijkstra;

import com.programminggeeks.graph.Node;
import com.programminggeeks.graphdroid.NormalizedCoordinates;

public class DijkstraNode extends Node implements Comparable<DijkstraNode> {

	/*
	 * It will have the shortest distance value
	 * from the Source Node to this Node
	 */
	public double d;
	
	/*
	 * The node which is just before the current node
	 * in the shortest path
	 */
	public DijkstraNode predecessor;
	
	public DijkstraNode(NormalizedCoordinates coordinates) {
		super(coordinates);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int compareTo(DijkstraNode another) {
		if(this.d > another.d)
			return 1;
		else if(this.d < another.d)
			return -1;
		return 0;
	}

}
