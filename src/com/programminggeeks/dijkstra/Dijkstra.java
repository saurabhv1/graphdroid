package com.programminggeeks.dijkstra;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Random;

import com.programminggeeks.graph.Edge;
import com.programminggeeks.graph.Graph;
import com.programminggeeks.graph.Node;
import com.programminggeeks.graph.WeightedDirectionalEdge;
import com.programminggeeks.graphdroid.NormalizedCoordinates;
import com.programminggeeks.util.Log;

public class Dijkstra {
	
	/*
	 * The Node from which we will determine the 
	 * shortest path to all the other nodes
	 */
	private Node sourceNode;
	
	/*
	 * The input graph on which we will run Dijkstra
	 */
	private Graph graph;
	
	private final float INFINITY = Float.POSITIVE_INFINITY;
	
	/*
	 * The list of nodes for which we have determined
	 * the shortest path from the sourceNode 
	 */
	private List<DijkstraNode> dijkstraNodes = new ArrayList<DijkstraNode>();
	
	private PriorityQueue<DijkstraNode> minQueue = new PriorityQueue<DijkstraNode>();
	
	private static final String TAG = Dijkstra.class.getSimpleName();
	
	public Dijkstra(Graph graph, Node sourceNode) {
		this.graph = graph;
		this.sourceNode = sourceNode;
		init();
	}
	
	public Dijkstra() {
		this.graph = buildSampleGraphForDijkstra();
	}
	
	public void runDijkstra() {
		init();
		buildShortestPath();
	}
	
	private void init() {
		// set the 'd' parameter for all the vertices (nodes) as INFINITY and 
		// that of source as 0
		List<Node> nodes = graph.getNodes();
		for(Node node : nodes) {
			if(!node.equals(sourceNode))
				((DijkstraNode)node).d = INFINITY;
			else
				((DijkstraNode)node).d = 0;
			
			// Add all the nodes to min-priority queue
			minQueue.add((DijkstraNode)node);
		}
	}
	
	private void buildShortestPath() {
		while(!minQueue.isEmpty()) {
			DijkstraNode u = minQueue.poll();
			dijkstraNodes.add(u);
			
			// for each vertex v belongs to G.Adj[u], Relax(u,v,w)
			List<Node> adjNodes = u.getAdjecentNodes();
			for(Node v : adjNodes) {
				relax(u,(DijkstraNode)v);
			}
		}
		// Now the minPriorityQueue contains the vertices having shortest path d parameter
		for(DijkstraNode node : dijkstraNodes) {
			Log.i(TAG,"Shortest distance for "+node.getNodeLabel()+":"+node.d);
			DijkstraNode predecessorNode = node;
			Log.i(TAG,"Path for "+node.getNodeLabel());
			List<DijkstraNode> nodesInShortestPath = new ArrayList<DijkstraNode>();
			while(predecessorNode != null) {
				//Log.i(TAG,predecessorNode.getNodeLabel());
				nodesInShortestPath.add(predecessorNode);
				predecessorNode = predecessorNode.predecessor;
			}
			Collections.reverse(nodesInShortestPath);
			for(DijkstraNode preNode : nodesInShortestPath)
				Log.i(TAG,preNode.getNodeLabel());
		}
	}
	
	/**
	 * Update the value of v.d if u.d+w(u,v) < v.d 
	 * @param u - the start node
	 * @param v - the end node
	 */
	private void relax(DijkstraNode u, DijkstraNode v) {
		WeightedDirectionalEdge edge = (WeightedDirectionalEdge) u.getEdge(v);
		double weight = edge.getWeight();
		if(v.d > u.d+weight) {
			v.d = u.d + weight;
			v.predecessor = u;
		}
	}
	
	public Graph buildSampleGraphForDijkstra() {
		
		Graph graph = new Graph();
		
		int numNodes = 5;
		List<DijkstraNode> nodes = new ArrayList<DijkstraNode>();
		List<WeightedDirectionalEdge> edges = new ArrayList<WeightedDirectionalEdge>();
		
		// create 5 nodes
		for(int x=0;x<numNodes;x++) {
			nodes.add(new DijkstraNode(new NormalizedCoordinates(new Random().nextInt(90),new Random().nextInt(90))));
			nodes.get(x).setNodeLabel(x+"");
		}
		
		// create edges
		WeightedDirectionalEdge edge = new WeightedDirectionalEdge(nodes.get(0),nodes.get(1),10,1);
		edge.setLabel(0+""); edges.add(edge);
		WeightedDirectionalEdge edge1 = new WeightedDirectionalEdge(nodes.get(0),nodes.get(4),5,1);
		edge1.setLabel(1+""); edges.add(edge1);
		
		WeightedDirectionalEdge edge2 = new WeightedDirectionalEdge(nodes.get(1),nodes.get(2),1,1);
		edge2.setLabel(2+""); edges.add(edge2);
		WeightedDirectionalEdge edge3 = new WeightedDirectionalEdge(nodes.get(1),nodes.get(4),2,1);
		edge3.setLabel(3+""); edges.add(edge3);
		
		WeightedDirectionalEdge edge4 = new WeightedDirectionalEdge(nodes.get(2),nodes.get(3),4,1);
		edge4.setLabel(4+""); edges.add(edge4);
		
		WeightedDirectionalEdge edge5 = new WeightedDirectionalEdge(nodes.get(3),nodes.get(2),6,1);
		edge5.setLabel(5+""); edges.add(edge5);
		WeightedDirectionalEdge edge6 = new WeightedDirectionalEdge(nodes.get(3),nodes.get(0),7,1);
		edge6.setLabel(6+""); edges.add(edge6);
		
		WeightedDirectionalEdge edge7 = new WeightedDirectionalEdge(nodes.get(4),nodes.get(3),2,1);
		edge7.setLabel(7+""); edges.add(edge7);
		WeightedDirectionalEdge edge8 = new WeightedDirectionalEdge(nodes.get(4),nodes.get(1),3,1);
		edge8.setLabel(8+""); edges.add(edge8);
		WeightedDirectionalEdge edge9 = new WeightedDirectionalEdge(nodes.get(4),nodes.get(2),9,1);
		edge9.setLabel(9+""); edges.add(edge9);
		
		graph.addNodes(nodes);
		graph.addEdges(edges);
		graph.createAdjacencyList();
		this.sourceNode = nodes.get(0);
		
		// Log the Adjecent Nodes
		for(DijkstraNode node : nodes) {
			for(Node adjNode : node.getAdjecentNodes()) {
				//Log.i(TAG,"Adj node for "+node.getNodeLabel()+" :"+adjNode.getNodeLabel());
			}
		}
		
		// Log outgoing edges
		for(DijkstraNode node : nodes) {
			for(Edge outGoingEdge : node.getAdjcentEdges()) {
				//Log.i(TAG,"Outgoing adge for "+node.getNodeLabel()+" :"+outGoingEdge.getNodes()[1].getNodeLabel());
			}
		}
		return graph;
	}
}
