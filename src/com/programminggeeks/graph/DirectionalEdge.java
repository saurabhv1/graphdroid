package com.programminggeeks.graph;

public class DirectionalEdge extends Edge {
	
	private int dir;
	
	public DirectionalEdge(Node n1, Node n2, int dir) {
		super(n1,n2);
		this.dir = dir;
	}

	public double getDir() {
		return dir;
	}
	
	public void reverseDir() {
		dir = dir * -1;
	}

}
