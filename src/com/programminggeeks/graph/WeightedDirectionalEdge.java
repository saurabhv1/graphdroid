package com.programminggeeks.graph;

public class WeightedDirectionalEdge extends Edge {
	
	private int dir;
	
	private double weight;
	
	public WeightedDirectionalEdge(Node n1, Node n2, double weight, int dir) {
		super(n1,n2);
		this.weight = weight;
		this.dir = dir;
	}
	
	public int getDir() {
		return dir;
	}
	
	public double getWeight() {
		return weight;
	}

}
