package com.programminggeeks.graph;

public class Edge {
	
	private Node n1;
	
	private Node n2;
	
	private String label;
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Edge(Node n1, Node n2) {
		this.n1 = n1;
		this.n2 = n2;
	}
	
	public Node[] getNodes() {
		Node[] nodes = {n1,n2};
		return nodes;
	}
	
	@Override
	public boolean equals(Object o) {
		return (this.n1.equals(((Edge)o).n1) && this.n2.equals(((Edge)o).n2));
	}
}
