package com.programminggeeks.graph;

public class WeightedEdge extends Edge {
	
	private double weight;
	
	public WeightedEdge(Node n1, Node n2, double weight) {
		super(n1,n2);
		this.weight = weight;
	}
	
	public double getWeight() {
		return weight;
	}

}
