package com.programminggeeks.graph.ui;

import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.programminggeeks.graph.Edge;
import com.programminggeeks.graph.Graph;
import com.programminggeeks.graph.Node;
import com.programminggeeks.util.GraphUtils;

public class GraphView extends SurfaceView implements SurfaceHolder.Callback {

	// The Graph to be drawn
	private Graph graph;

	private SurfaceHolder sh;
	private final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private final Paint paintText = new Paint(Paint.ANTI_ALIAS_FLAG);
	private final Paint paintArrow = new Paint(Paint.ANTI_ALIAS_FLAG);


	public GraphView(Context context, Graph graph) {
		super(context);
		this.graph = graph;
		sh = getHolder();
		sh.addCallback(this);
		paint.setColor(Color.BLUE);
		paint.setStyle(Style.FILL);
		
		paintText.setColor(Color.RED);
		paintText.setStyle(Style.FILL_AND_STROKE);
		paintText.setTextSize(GraphUtils.convertToDp(70));
		
		paintArrow.setColor(Color.MAGENTA);
		paintArrow.setStyle(Style.FILL);
		
	}


	public void surfaceCreated(SurfaceHolder holder) {
		Canvas canvas = sh.lockCanvas();
		canvas.drawColor(Color.WHITE);
		//canvas.drawCircle(100, 200, 50, paint);
		drawGraph(canvas);

		sh.unlockCanvasAndPost(canvas);
	}

	private void drawGraph(Canvas canvas) {
		List<Edge> edges = graph.getEdges();

		for(Edge edge : edges) {

			Node node1 = edge.getNodes()[0];
			Node node2 = edge.getNodes()[1];

			float x1 = GraphUtils.toPhoneXCoordinate(node1.getX());
			float y1 = GraphUtils.toPhoneYCoordinate(node1.getY());
			float x2 = GraphUtils.toPhoneXCoordinate(node2.getX());
			float y2 = GraphUtils.toPhoneYCoordinate(node2.getY());

			canvas.drawCircle(x1,y1, 10, paint);
			canvas.drawText(node1.getNodeLabel(), x1, y1, paintText);
			canvas.drawCircle(x2,y2, 10, paint);
			canvas.drawText(node2.getNodeLabel(), x2, y2, paintText);
			
			double slope = (y2-y1)/(x2-x1);
			double theta = Math.atan(slope);
			
			float r = GraphUtils.convertToDp(70);
			
			float arrowX1, arrowY1, arrowX2, arrowY2;
			
			if((y2 > y1 && x2 < x1) || (y2 < y1 && x2 < x1)) {
				arrowX1 = (float) (x2 + r * Math.cos(theta - Math.PI/6));
				arrowY1 = (float) (y2 + r * Math.sin(theta - Math.PI/6));
				arrowX2 = (float) (x2 + r * Math.cos(theta + Math.PI/6));
				arrowY2 = (float) (y2 + r * Math.sin(theta + Math.PI/6));
			}
			
			else {
				arrowX1 = (float) (x2 - r * Math.cos(theta - Math.PI/6));
				arrowY1 = (float) (y2 - r * Math.sin(theta - Math.PI/6));
				arrowX2 = (float) (x2 - r * Math.cos(theta + Math.PI/6));
				arrowY2 = (float) (y2 - r * Math.sin(theta + Math.PI/6));
			}

			
			canvas.drawLine(arrowX1,arrowY1, x2, y2, paintArrow);
			canvas.drawLine(arrowX2,arrowY2, x2, y2, paintArrow);
			
			canvas.drawLine(x1,y1,x2,y2, paint);
			//canvas.drawText(edge.getWeight()+"", (x1+x2)/2, (y1+y2)/2, paintText);
		}
	}


	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
	}


	public void surfaceDestroyed(SurfaceHolder holder) {

	}

}
