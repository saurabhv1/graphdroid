package com.programminggeeks.graph;

import java.util.ArrayList;
import java.util.List;

import com.programminggeeks.graphdroid.NormalizedCoordinates;

public class Node {
	
	private String nodeLabel;
	
	private float x;
	
	private float y;
	
	// Each node will have a list of nodes to which this is connected
	List<Node> adjecentNodes = new ArrayList<Node>();
	
	// Each node will maintain the edges going out from this node
	List<Edge> adjecentEdges = new ArrayList<Edge>();
	
	
	public Node(NormalizedCoordinates coordinates) {
		this.x = coordinates.getX();
		this.y = coordinates.getY();
	}

	public String getNodeLabel() {
		return nodeLabel;
	}

	public void setNodeLabel(String nodeLabel) {
		this.nodeLabel = nodeLabel;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}
	
	public int getDegree() {
		return this.adjecentNodes.size();
	}
	
	public Edge getEdge(Node n2) {
		if(n2 == null)
			return null;
		if(this.adjecentNodes.contains(n2)) {
			int edgeNo = 0;
			for(Node node : adjecentNodes) {
				if(node.equals(n2)) {
					return this.adjecentEdges.get(edgeNo);
				}
				edgeNo++;
			}
		}
		return null;
	}
	
	public List<Node> getAdjecentNodes() {
		return adjecentNodes;
	}
	
	public List<Edge> getAdjcentEdges() {
		return adjecentEdges;
	}
	
	@Override
	public boolean equals(Object o) {
		Node node = (Node)o;
		return (node.x == this.x && node.y == this.y);
	}

}
