package com.programminggeeks.graph;

import java.util.ArrayList;
import java.util.List;

import com.programminggeeks.dijkstra.DijkstraNode;
import com.programminggeeks.graphdroid.NormalizedCoordinates;

public class Graph {
	
	private List<Edge> edgesList = new ArrayList<Edge>();
	private List<Node> nodesList = new ArrayList<Node>();
	
	public Graph() {
		// TODO Auto-generated constructor stub
	}
	
	public void addEdge(Edge edge) {
		edgesList.add(edge);
	}
	
	public void addEdges(List<? extends Edge> edges) {
		edgesList.addAll(edges);
	}
	
	public List<Edge> getEdges() {
		return edgesList;
	}
	
	public void removeEdge(Edge edge) {
		edgesList.remove(edge);
	}
	
	public void addNodes(List<? extends Node> nodes) {
		nodesList.addAll(nodes);
	}
	
	public List<Node> getNodes() {
		return nodesList;
	}
	
	public boolean doesEdgeExists(Edge edge) {
		return edgesList.contains(edge);
	}
	
	public boolean doesNodeExists(Node node) {
		return nodesList.contains(node);
	}
	
	/**
	 * Create the adjacency list representation of the graph.
	 * Loop through all the edges and get the starting node, node[0] for this edge. 
	 * Now, add all the nodes which are adjacent to node[0] to the list {@link com.programminggeeks.graph.Node#adjecentNodes}
	 * and add all the edges going out of node[0] to the list {@link com.programminggeeks.graph.Node#adjecentEdges}
	 */
	public void createAdjacencyList() {
		for(Edge edge : edgesList) {
			Node node[] = edge.getNodes();
			node[0].adjecentNodes.add(node[1]);
			node[0].adjecentEdges.add(edge);
		}
	}

}
